#!/usr/bin/env python2

import shutil
import os
import sys
import os.path
import zipfile
import argparse

try:
    import chardet
    DEFAULT_ENCODING = 'chardet'
except ImportError:
    print >> sys.stderr, 'WARNING: "chardet" module not installed, encoding detection disabled'
    DEFAULT_ENCODING = 'utf-8'

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description="unzip a file")
    parser.add_argument('-d', '--dir', help='directory, in which to extract. Defaults to current directory')
    parser.add_argument('--enc', '--encoding', default=DEFAULT_ENCODING, help='Encoding of the file. Defaults to "%s"' % DEFAULT_ENCODING)
    parser.add_argument('file')

    args = parser.parse_args()
    
    zfile = zipfile.ZipFile(args.file)

    encoding = args.enc

    if encoding == 'chardet' and not chardet:
        sys.stderr.write('No chardet module available,'
                         ' please, use another encoding')
        sys.exit(1)

    for zinfo in zfile.filelist:

        filename = zinfo.filename

        # if encoding not yet detected
        if encoding == 'chardet':
            try:
                # if the filename is plain ASCII, just skip it
                filename = filename.decode()
            except UnicodeDecodeError:
                detected_encoding = chardet.detect(zfile.filelist[0].filename)
                if detected_encoding['confidence'] >= 0.8:
                    print >> sys.stderr, 'Detected encoding %(encoding)s with'\
                                     ' confidence %(confidence)s' % \
                                     detected_encoding
                    encoding = detected_encoding['encoding']
                else:
                    print >> sys.stderr, 'Cannot detect the encoding with chardet, '\
                                     'please provide the encoding via --enc'
                    sys.exit(1)
                filename = filename.decode(encoding)
        else:
            filename = filename.decode(encoding)

        filename = filename.encode('utf-8')

        
        if args.dir:
            filename = os.path.join(args.dir, filename)

        # Create the missing directories
        dirname = os.path.dirname(filename)
        try:
            os.makedirs(dirname)
        except OSError, e:
            if e.errno != 17:
                continue
                print >> sys.stderr, e

        # The file is a dir, continue
        if filename.endswith('/'):
            continue
        with zfile.open(zinfo) as src:
            with open(filename, 'wb') as dst:
                shutil.copyfileobj(src, dst)
